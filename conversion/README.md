# Convertisseur entre différents schémas json

## Pourquoi ?
Le crawler permet de générer une liste de Chatons dans un format qui ressemble à celui des fiches (par exemple, https://chatons.org/fr/chaton/artifaille-0). Mais, une fois que l'on a l'information, on peut la transformer et la mettre dans un autre format. Ainsi, ce système est interopérable et l'on peut utiliser les données pour communiquer avec d'autres sites ou nourrir d'autres bases.

## Usage

Pour l'instant, le convertisseur prend en entrée un fichier JSON conforme à notre schéma et sort un JSON qui correspond aux données d'[entraide](https://entraide.chatons.org).

Il s'utilise ainsi :

```
converter.py [-o OUTPUT] input
```

## Fonctionnement
La conversion s'effectue en trois étapes.
Dans les étapes qui vont suivre, on considère un fichier décrivant le Chaton suivant :
```json
{
  "name": "Picasoft",
  "url": "https://picasoft.net",
  "services": [
    {
      "type": "pad_collaboratif",
      "url": "https://week.pad.picasoft.net",
      "software": "etherpad",
      "account": "sans_inscription",
      "public": "tou.te.s",
      "price": "gratuit",
      "degradability": 7,
      "weight": 1
    },
    {
      "type": "messagerie_instantanee",
      "description": "Instance Mattermost pour communiquer en équipe",
      "url": "https://team.picasoft.net",
      "software": "mattermost",
      "account": "sur_inscription",
      "public": "tou.te.s",
      "price": "gratuit",
      "degradability": 0,
      "weight": 5
    }
  ],
  "structure": "association",
  "status": "approuve"
}

```
### Renommage des clés, conversion des valeurs
Le json d'entrée, sous forme de liste ou d'objet, est parcouru en profondeur d'abord, et les champs sont renommés et leur contenu adapté sans transformer la structure du json. Cette conversion est décrite dans le fichier [data.py](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/conversion/data.py), dictionnaire qui explicite comment doivent se transformer les valeurs.
Le fichier décrit devient :
```json
{
  "chaton": "Picasoft",
  "chaton_website": "https://picasoft.net",
  "services": [
    {
      "endpoint": "https://week.pad.picasoft.net",
      "software": "Etherpad",
      "degradability": "6 mois",
      "weight": 1
    },
    {
      "endpoint": "https://team.picasoft.net",
      "software": "Mattermost",
      "degradability": "Jamais",
      "weight": 5
    }
  ]
}
```
Les clés ont été renommées (`name` qui devient `chaton`) et les valeurs converties (les urls sont laissés intacts, le champ `software` anciennement `"etherpad"` devient `"Etherpad"`, la `degradability` passe de `0` à `"Jamais"`...). Les champs non-utiles pour le schéma entraide sont laissés de côté.
Un système de *contexte* permet de savoir par exemple si l'`url` trouvé correspond à celui du Chaton (`"https://picasoft.net"`) ou du service rendu (`"https://team.picasoft.net"`).

### Explosion du Chaton par services
Dans le format entraide, chaque objet correspond à un service donné par un Chaton. Pour chaque Chaton, pour chaque service on crée un objet qui résume l'offre :
```json
[
  {
    "chaton": "Picasoft",
    "chaton_website": "https://picasoft.net",
    "type": "Rédaction collaborative (alternative à Google Docs)",
    "endpoint": "https://week.pad.picasoft.net",
    "software": "Etherpad",
    "degradability": "6 mois",
    "weight": 1
  },
  {
    "chaton": "Picasoft",
    "chaton_website": "https://picasoft.net",
    "type": "Aucune description pour ce service",
    "endpoint": "https://team.picasoft.net",
    "software": "Mattermost",
    "degradability": "Jamais",
    "weight": 5
  }
]
```
Certains champs sont ajoutés, comme le champ `type` qui décrit ce que permet le service, description générée en fonction du service, dont les règles sont également contenues dans le dictionnaire [data.py](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/conversion/data.py).
### Conversion au format node
Enfin, on *node-ifie* la liste obtenue afin qu'elle corresponde à la structure node d'entraide.chatons.org :
```json
{
  "nodes": [
    {
      "node": {
        "chaton": "Picasoft",
        "chaton_website": "https://picasoft.net",
        "type": "Rédaction collaborative (alternative à Google Docs)",
        "endpoint": "https://week.pad.picasoft.net",
        "software": "Etherpad",
        "degradability": "6 mois",
        "weight": 1
      }
    },
    {
      "node": {
        "chaton": "Picasoft",
        "chaton_website": "https://picasoft.net",
        "type": "Aucune description pour ce service",
        "endpoint": "https://team.picasoft.net",
        "software": "Mattermost",
        "degradability": "Jamais",
        "weight": 5
      }
    }
  ]
}
```

Et le tour est joué.
