#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import json
import convert_json


parser = convert_json.makeParser()
args = parser.parse_args()

# Ouverture des fichiers IO
json_input_file = args.input.name
json_output_file = (
    args.output.name
    if args.output is not None
    else os.path.splitext(json_input_file)[0] + "_converted.json"
)

output_file = open(json_output_file, "w", encoding="utf8")
with open(json_input_file, "r", encoding="utf8") as input_file:
    input = json.loads(input_file.read())

liste_chatons = convert_json.convertSchema(input)

json.dump(
    liste_chatons,
    output_file,
    indent=2,
    ensure_ascii=False,
)
output_file.close()
