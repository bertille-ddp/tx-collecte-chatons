# ToDo list

- [X] Mettre la sortie en ligne
- [ ] Supporter tous les formats
- [ ] N'exporter que les Chatons valides pour entraide.chatons.org
- [ ] S'occuper du champ `edit_link`
- [x] Rendre vraiment conforme au schéma entraide (nodes)
- [ ] Proposer de choisir le type d'export (vers schema.org par exemple)
