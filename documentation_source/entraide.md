# Une instance entraide
Notre instance entraide est [disponible ici](http://chatons.picasoft.net:8000/).

## Qu'est-ce que c'est ?
Pour rechercher facilement un Chaton sur le seul critère des services proposés, plutôt que d'utiliser l'[outil de recherche](https://chatons.org/find) complet, il est possible d'utiliser un service simplifié et visuel : [entraide](https://entraide.chatons.org/).

En utilisant les données de description des Chatons, nous avons nous aussi monté notre [instance entraide](http://chatons.picasoft.net:8000/) en guise de *PoC*.


## Fonctionnement
Une fois le [fichier json](http://chatons.picasoft.net/exports/chatons.json) généré par le [crawler](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/crawler), il est possible de convertir ce type de json en un [autre json](http://chatons.picasoft.net/exports/chatons-services.json) contenant certes moins de données, mais l'essentiel pour l'intance entraide. Cette conversion est faite grâce à notre outil le [convertisseur](http://chatons.picasoft.net/documentation/convertisseur/).  
Il ne reste plus qu'à mettre en ligne notre instance grâce à l'outil entraide, disponible sur [le framagit](https://framagit.org/chatons/entraide) des Chatons.

Notre instance est [disponible ici](http://chatons.picasoft.net:8000/).
