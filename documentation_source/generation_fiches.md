# Un outil pour créer plus facilement la fiche d'un Chaton

Le générateur est [disponible ici](http://chatons.picasoft.net/gen/json_gen.html).

## Utilisation
Une fiche d'un Chaton ressemble à ça :

```json
{
  "name": "Picasoft",
  "description": "L’Association Picasoft a pour objet de promouvoir et défendre une approche libriste respectueuse de la vie privée et de la liberté d’expression dans le domaine de l’informatique.",
  "url": "https://picasoft.net",
  "logo": "https://doc.picasoft.net/res/picasoft_logo_alt.png",
  "structure": "association",
  "status": "approuve",
  "servers": [ "https://tetaneutral.net/" ],
  "economic_model": [ "don", "adhesion", "subvention", "autre" ],
  "public": [ "tou.te.s", "adherent.e.s" ],
  "creation_date": "2016",
  "address": "rue Roger Couttolenc, 60200 Compiegne",
  "perimeter": "Oise",
  "services": [
    {
      "type": "messagerie_instantanee",
      "description": "Instance Mattermost pour communiquer en équipe",
      "url": "https://team.picasoft.net",
      "software": "mattermost",
      "account": "sur_inscription",
      "public": "tou.te.s",
      "price": "gratuit",
      "degradability": 0,
      "weight": 5
    }
  ]
}
```
Pour éviter de devoir écrire un tel fichier à la main, nous avons mis en place un [générateur de fiche](http://chatons.picasoft.net/gen/json_gen.html).

C'est en fait un formulaire qui est lui-même généré automatiquement à partir du [schéma](schema.md) grâce à la librairie [json-forms](https://github.com/brutusin/json-forms) développée par [Brutusin](https://brutusin.org).

La librairie est appelée grâce à un bout de code en JavaScript inséré dans la [page du générateur](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/site/gen/json_gen.html).
