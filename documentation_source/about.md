Qui sommes-nous ?
=================

Nous sommes deux étudiant·es de l'Université de Technologie de Compiègne. Notre école propose à ses élèves de réaliser des études expérimentales, ou TX. Ce sont des projets en semi-autonomie portant sur des sujets «&nbsp;réels&nbsp;» et pas seulement pédagogiques. C'est dans ce cadre-là que nous avons conçu cet outil.

Remerciements
=============

Merci à Stéphane Crozat, Rémi Uro et Quentin Duchemin d'avoir encadré ce projet.

Merci à Angie et à pyg d'avoir répondu à nos questions et de nous avoir aidés à comprendre l'écosystème du CHATONS.

Merci à TTDM, ljf, djayroma, unteem, numahell, Neil, Kyâne, Meewan, pierre, bouviermullerp, mrflos, cpm, deblan, Audrey et Mablr pour leurs coups de main, retours et suggestions.
