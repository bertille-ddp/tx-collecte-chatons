# Une liste de ce qui ne sera pas achevé

Le temps de la est terminé et un certain nombre de chantiers sont encore en cours. Nous les listons ici pour qu'ils ne soient pas oubliés.

## Sur le schéma

* À l'heure actuelle, les valeurs sont des étiquettes en français approximatif. Par exemple, une des valeurs possibles pour le type de service est : `partage_temporaire_fichier`. Il serait plus pertinent de choisir soit une étiquette en anglais (`temporary_file_sharing`), soit directement la donnée en français (`Partage temporaire de fichiers (alternative à WeTransfer, DropSend)`) comme c'est le cas dans [les fiches de chatons.org](https://entraide.chatons.org/page-data/fr/page-data.json).

  Une synthèse sur cette réflexion est disponible [à la page suivante](formatage_donnees.md) et pourrait aboutir à une version 0.0.3 du schéma (ou même à une 0.1 ?).
* Le schéma actuel n'est pas exhaustif. Certaines informations des fiches des Chatons, qui ne sont pas forcément nécessaires à l'utilisateur·ice final·e mais sont utiles à la vie du collectif, manquent. Par exemple, le schéma ne permet pas pour l'instant de détailler le respect de la charte point par point.
* De façon générale, la modélisation est amenée à évoluer en même temps que le collectif.

## Sur la conversion

* Pour l'instant, on ne peut convertir les données récoltées qu'au format d'entraide.chatons.org. Il serait intéressant de pouvoir exporter les données dans le format Organization de schema.org, pour accéder à l'interopérabilité du web sémantique. Le sujet est détaillé [dans cette section](jsonld.md).

* On pourrait imaginer encore d'autres formats d'export, et donc de rendre la conversion plus modulable. Est-il possible d'adapter le script existant en ce sens, ou vaut-il mieux repartir d'un autre outil ? La question est ouverte. En tout cas, l'outil de conversion pourrait gagner en clarté.

## Sur le crawler

* Pour l'instant, le crawler utilise un module de logging maison. Il faudrait plutôt utiliser le module natif de Python, et donc passer du temps à examiner quel comportement est souhaité et comment l'obtenir.

* De manière générale, le code pourrait être plus propre.

## Sur le site

* Le site mériterait un coup de CSS pour être plus joli s'il doit être gardé, ou d'être intégré à chatons.org sinon.

* [L'outil de recherche](http://chatons.picasoft.net/search/recherche.html), en particulier, n'est pas très convivial et ne permet pas d'interroger tous les champs.

* [L'outil de génération de fiche](<http://chatons.picasoft.net/gen/json_gen.html>) pourrait être refait maison.

## Sur le passage au collectif

* Pour l'instant, tout est sur un Framagit personnel mais le but est de laisser la main au CHATONS si la preuve de concept séduit. Dans ce cas, il faudrait tout transférer sur [le groupe du collectif](http://chatons.picasoft.net/gen/json_gen.html), s'assurer que la documentation est suffisante et rester disponibles. La question de l'intégration technique et fonctionnelle à https://chatons.org reste à imaginer.
