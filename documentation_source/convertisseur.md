# Le convertisseur vers entraide

Le convertisseur est [disponible ici](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/conversion).  

Le json converti est [disponible ici](http://chatons.picasoft.net/exports/chatons-services.json).  

L'instance entraide est [disponible ici](http://chatons.picasoft.net:8000/).  

## Principe

Les données récupérées par le crawler permettent de créer les fiches des Chatons pour [le site principal](https://chatons.org).

Mais, avec ces informations, il est possible de faire plein d'autres choses. Par exemple, on peut les exporter de telle sorte à fournir les informations nécessaires au [site d'entraide](https://entraide.chatons.org). On peut aussi en faire des données sémantiques pour décrire une [organisation telle que pensée par schema.org](https://schema.org/Organization).


## Utilisation
Son utilisation se fait en ligne de commande :
```
converter.py [-o OUTPUT] <input>
```
Avec `<input>` le [fichier json](http://chatons.picasoft.net/exports/chatons.json) exporté par le [crawler](http://chatons.picasoft.net/documentation/crawler/), et output un fichier dans lequel écrire l'export (par défaut `<input>_converted.json`)

## Fonctionnement
(Le fonctionnement détaillé est donné sur le [Framagit](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/conversion#fonctionnement).)

La conversion se fait en trois étapes :  

- Renommage des clés et des valeurs (voir [pourquoi les clefs et les valeurs doivent être converties](formatage_donnees.md)),  
- Explosion des Chatons par service,  
- Transformation de la structure vers du Node.  

## Fonctionnalité de l'export
Nous avons pu tester que l'export ainsi obtenu fonctionnait bien en faisant tourner [le site d'entraide](https://framagit.org/chatons/entraide). Cependant, le site donne des erreurs s'il n'y a aucun fournisseur pour certains services. Nous avons donc dû ajouter à la [liste](centralisation.md) un Chaton factice, dont les données servent seulement de démonstration pour l'export entraide.

Sur notre site le convertisseur tourne juste après le [crawler](http://chatons.picasoft.net/documentation/crawler/) et le json ainsi converti est disponible à [cette adresse](http://chatons.picasoft.net/exports/chatons-services.json).

Notre instance entraide utilisant ces données est [documentée ici](http://chatons.picasoft.net/documentation/entraide/) et fonctionne en ligne à [cette adresse](http://chatons.picasoft.net:8000/).
