Comment marche ce site
======================

Principe
--------

Les sources du site sont hébergées sur le [Framagit du projet](https://framagit.org/bertille/tx-collecte-chatons) dans un dossier spécifique `site`.

Un serveur HTTP a été mis en place avec NGINX (grâce, notamment, à [ce tutoriel](https://medium.com/@jgefroh/a-guide-to-using-nginx-for-static-websites-d96a9d034940))&nbsp;; il a été configuré pour le site `chatons.picasoft.net`, en indiquant que le contenu se trouvait dans `/var/www/html/chatons`&nbsp;; c'est en fait un lien symbolique vers `/home/tx/tx-collecte-chatons/site`.

Grâce à un `crontab`, le site est mis à jour toutes les nuits autour de 2h du matin. Un `git pull` télécharge toutes les sources, puis la documentation est regénérée, le crawler et le convertisseur exécutés, et les logs archivés.

La routine exécutée est :
```bash
cd ~/tx-collecte-chatons
git checkout master && git pull
mkdocs build
cd crawler
./run_crawler.sh
cd ../conversion
./run_convert.sh
cd ../site/logs
cat log.txt > log_archive.txt
cd ../entraide
killall node
gatsby clean
gatsby build
gatsby serve -H chatons.picasoft.net -p 8000 &
```

Documentation
-------------

La partie documentation du site est générée avec [MkDocs](https://www.mkdocs.org/). Les fichiers Markdown du dossier `documentation_source` sont convertis en HTML et ajoutés au dossier `site/documentation`.

Recherche
---------

La partie exploration des Chatons est structurée avec des liens symboliques. Ils permettent d'accéder aux exports JSON (du dossier `site/exports` à `sortie`) et aux logs (de `site/logs/log.txt` à `crawler/log.txt`), qui par défaut sont générés dans le dossier du crawler. Un autre lien pointe vers le schéma.

Le fichier `recherche.html` fait appel aux fonctions de `query.js`, qui chargent le schéma, les données des Chatons et construisent le formulaire puis les résultats de la recherche.
