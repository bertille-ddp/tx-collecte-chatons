# Liste des URL à vérifier
Le crawler pourrait en théorie fonctionner avec n'impote quelle liste d'URL et n'importe quel schéma. Cependant, le cas qui nous intéresse est celui des CHATONS, nous rassemblons donc les adresses de tous les fichiers qui les décrivent.

Cette liste est dans le fichier `CHATONS_list.txt`.

Chaque ligne non commentée devrait être une URL et précédée d'un commentaire qui indique à quel CHATON ce fichier correspond.

## Un fichier exemple
`exemple.txt` peut montrer à quoi ressemble un fichier de liste, mais surtout il contient les adresses vers quelques fichiers, certains valides et d'autres non. De cette manière, il est possible de tester le crawler et les différentes sorties qu'il donne.
