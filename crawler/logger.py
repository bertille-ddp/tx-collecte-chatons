#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime


class Log:
    def __init__(self, path, verbose=False):
        self.logfile = open(path, "a", encoding="utf8")
        self.verbose = verbose

    def __del__(self):
        self.logfile.close()

    def _date(self):
        now = datetime.now()
        return now.strftime("[%Y/%m/%d-%H:%M:%S]")

    def _write(self, text):
        self.logfile.write(self._date() + " " + text + "\n")

    def _show(self, text, indent=0):
        print(" " * 2 * indent + text)

    def log(self, text):
        self._write("  [LOG] " + text)

    def info(self, text, indent=0):
        self._write(" [INFO] " + text)
        self._show("- " + text, indent)

    def infoVerbose(self, text, indent=0):
        self._write(" [INFO] " + text)
        if self.verbose:
            self._show("- " + text, indent)

    def error(self, text, indent=0, onlyLog=False):
        self._write("[ERROR] " + text)
        if not onlyLog:
            self._show("! " + text, indent)

    def debug(self, text, indent=0):
        self._write("[DEBUG] " + text)
        self._show("# " + text, indent)

    def urlNotParsed(self, text, indent=0):
        self.error("URL not parsed : " + text, indent)
