#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ./crawler.py -s ../schema/CHATONS_schema.json -i ../liste/CHATONS_list.txt

import os
import sys
import json
from jsonschema import validate, ValidationError, SchemaError
import json_chaton
import logger


# Test de version
MIN_PYTHON = (3, 6)
if sys.version_info < MIN_PYTHON:
    sys.exit("This script require python version >= %s.%s\n" % MIN_PYTHON)

# Ouverture du fichier de log
logdir = os.path.dirname(os.path.realpath(__file__))
log = logger.Log(os.path.join(logdir, "log.txt"))
log.log(f"Begin session : {' '.join(sys.argv)}")

# Définition de l'ArgumentParser
parser = json_chaton.makeParser()

# Lecture des arguments I/O
try:
    args = parser.parse_args()
except SystemExit:
    exc = sys.exc_info()[1]
    # print(exc)
    log.error("Invalid arguments", 0, True)
    log.log("End session")
    sys.exit(0)

# Màj du mode du crawler si mode verbeux
if args.verbose:
    log.verbose = True

# Ouverture des fichiers IO
json_input_file = args.input.name
json_output_file = (
    args.output
    if args.output is not None
    else os.path.splitext(json_input_file)[0] + ".json"
)
json_schema_file = args.schema.name if args.schema is not None else None

#   Ouverture des fichiers donnés en argument :
# Schéma
json_key = None
if json_schema_file:
    with open(json_schema_file, "r", encoding="utf8") as schema_file:
        try:
            schema = json.loads(schema_file.read())
        except json.decoder.JSONDecodeError:
            log.error("Ill-formed JSON schema")
            log.info("List not parsed")
            log.log("End session")
            sys.exit(0)
        else:
            try:
                json_key = schema["required"][0]
            except (KeyError, IndexError):
                pass

# Sortie
json_old = set()
try:
    output_file = open(json_output_file, "r", encoding="utf8")
    json_old = {
        json_chaton.Chaton(json_key, data)
        for data in json.loads(output_file.read())
    }
except (FileNotFoundError, json.JSONDecodeError):
    # Pas d'ancien fichier, ou fichier mal formé : on ignore
    pass

output_file = open(json_output_file, "w", encoding="utf8")

# Préparation du set de stockage des données json collectées
json_actual = set()

# Générateur d'urls d'après le fichier d'entrée
urls = (
    url
    for url in (
        line.split("\n")[0].lstrip()
        for line in open(json_input_file, "r", encoding="utf8")
    )
    if url and not url.startswith("#")
)
# Boucle sur les URL trouvées
nb_total = 0
for url in urls:
    nb_total += 1
    log.info(f"Doing : {url}")

    try:
        data = json_chaton.urlToJSON(url)
    except json_chaton.NormalizeException:
        log.urlNotParsed(f"Problem normalizing {url}", 1)
        continue
    except json_chaton.UrlOpeningException:
        log.urlNotParsed(f"Problem opening {url}", 1)
        continue
    except json.JSONDecodeError:
        log.urlNotParsed(f"Ill-formed JSON file in {url}", 1)
        continue

    if data is None:
        log.urlNotParsed(f"No data in JSON file in {url}", 1)
        continue

    if json_schema_file:
        try:
            validate(instance=data, schema=schema)
        except ValidationError:
            log.urlNotParsed("Invalid JSON according to given schema", 1)
            continue
        except SchemaError:
            log.debug("Should not happen : validating schema")
            continue

    # Création de l'objet Chaton et ajout au grand set
    chaton = json_chaton.Chaton(json_key, data)
    if chaton in json_actual:
        log.urlNotParsed("Chaton already exists", 1)
        continue

    json_actual.add(chaton)
    log.info("Done", 1)

print()
log.info(
    f"Parsed {len(json_actual)}/{nb_total} url{'s' if nb_total>1 else ''}"
)

# Affichage du changelog
print()
json_removed = set()
if json_old:
    # Recherche des Chatons supprimés
    json_removed = json_old - json_actual
    if not args.update_only:
        json_chaton.showChatonListIfVerbose("Removed", json_removed, log)
    # Recherche des Chatons nouveaux, mis à jour, intouchés

    json_new = json_actual - json_old
    json_updated, json_untouched = json_chaton.getUpdatedAndUntouched(
        json_old, json_actual
    )
    json_chaton.showChatonListIfVerbose("Added", json_new, log)
    json_chaton.showChatonListIfVerbose("Updated", json_updated, log)
    json_chaton.showChatonListIfVerbose("Untouched", json_untouched, log)
else:
    json_chaton.showChatonListIfVerbose("Added", json_actual, log)

# Ajout des anciens chatons si besoin
if args.update_only and len(json_removed) > 0:
    print()
    print("- There are removed entries, they will added in the new file")
    json_chaton.showChatonListIfVerbose("Kept", json_removed, log)
    if not json_schema_file:
        json_actual = json_actual | json_removed
    else:
        # Validation des anciens Chatons par le schema
        json_v, json_i = json_chaton.validateOldChatons(json_removed, schema)
        json_chaton.showChatonListIfVerbose("Valid", json_v, log)
        json_chaton.showChatonListIfVerbose("Invalid", json_i, log)
        json_actual = json_actual | json_v

# Ecriture dans le fichier de sortie
print()
log.info(
    f"Writing {len(json_actual)} Chaton{'s' if len(json_actual)>1 else ''} \
in {json_output_file}"
)
json.dump(
    [chaton.data for chaton in json_actual],
    output_file,
    indent=2,
    ensure_ascii=False,
)
output_file.close()
log.info("Done", 1)

log.log("End session")
