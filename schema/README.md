# Principe du schéma
Le schéma CHATONS\_schema.json a vocation à valider que la fiche d'un Chaton contient les bonnes informations. Pour l'instant, il permet de valider un document JSON « simple » et pas JSON-LD, c'est une piste possible d'amélioration.


# Définition des champs
Les données sont structurées avec un système clé/valeur. Pour correspondre au web sémantique les clés sont exprimées en anglais, et les valeurs sont en français. Les valeurs sont en général à choisir dans une liste prédéfinie. Il faudrait pouvoir ajouter des valeurs à ces listes sans compromettre l'intégrité du modèle.modèle sans en compromettre l'intégrité est à se poser.

Sur la base des repérages faits au début du projet, nous avons pu établir le modèle suivant :

```plantuml
hide circle


class c as "Chaton" {
  # url : String
  - name : String
  - description : String
  - logo : String
  - creation_date : String
  - address : String
  - perimeter : String
  - structure : String
  - status : String
  - public : [String]
  - economic_model : [String]
  - servers : [Sring]
  - services : [Service]
  - metadata : Object
}

class S as "Service" {
  - type : String
  - description : String
  - url : String
  - software : String
  - account : String
  - public : String
  - price : String
  - degradability : Int
  - maintained_till : Date
  - weight : Int
  - metadata : Object
}
```

## Champs obligatoires
Sur le site actuel du collectif, les champs obligatoires sont le **nom**, la **structure** et le **statut**. Nous partons donc de cette base et rendons aussi obligatoire l'**URL**, qui servira d'identifiant.

Pour l'instant, les noms des services sont contraints, ce qui permet de s'assurer de la cohérence dans la dénomination d'un même service. Néanmoins, cela devrait peut-être être assoupli puisque le site actuel du CHATONS permet de saisir un service personnalisé. En tout cas, les merge request sont les bienvenues pour enrichir la liste de services.

Dans des versions futures, il sera sans doute intéressant de rendre obligatoires le public (au niveau du Chaton) et le modèle économique (idem). De la même manière, on pourrait rendre obligatoire l'URL des services pour avoir une base de données qui permette d'alimenter entraide.chatons.org.

L'adresse est peut-être plus délicate puisqu'un Chaton peut avoir un siège à un endroit et des serveurs à un autre. Idem pour l'hébergeur, qui n'est sans doute pas pertinent si le Chaton est auto-hébergé.

Enfin, il ne devrait pas être obligatoire de renseigner le public et le tarif pour chaque service, sous peine de devenir trop lourd.

## Détails des champs d'un Chaton
### url
C'est l'adresse web du Chaton.

### description
Une courte présentation de la structure et de ses activités.

### logo
Un lien vers le logo du Chaton.

### creation_date
La date de création du Chaton, au format "AAAA", "AAAA-MM" ou "AAAA-MM-JJ".

### address
L'adresse de son siège.

### perimeter
Pour renseigner le périmètre des actions du Chaton, par exemple la zone géographique à laquelle il propose préférentiellement ses services ou ses autres activités.

### structure
La forme juridique du Chaton. Elle prend une valeur parmi une liste déjà définie :
- "association" : pour les associations loi 1901 ;
- "collectif" : pour les groupes informels ;
- "cooperative" : pour les SCOP et autres entreprises coopératives ;
- "micro\_entreprise" : pour les entreprises de moins de 10 personnes, à l'exception de celles d'une seule ;
- "entreprise" : pour les autres entreprises ;
- "particulier" : pour les projets individuels ;
- "auto-entrepreneur.euse" : pour les projets individuels à but lucratif ou sous statut d'entreprise ;
- "autre" : pour ce qui ne rentre pas dans une des catégories énoncées plus haut.

### economic_model
Comment le Chaton se finance. Il est possible de cumuler plusieurs valeurs parmi :
- "gratuit" : certains services sont gratuits ;
- "payant" : certains services sont payants ;
- "prix\_libre" : certains services sont à prix libre ;
- "freemium" : certains services sont en freemium ;
- "adhesion" : la structure demande des adhésions à ses membres (certains services peuvent être réservés aux adhérent.e.s) ;
- "don" : la structure accepte les dons ou en reçoit beaucoup ;
- "subvention" : la structure est subventionnée ;
- "autre".

### public
Qui peut utiliser les services du Chaton. Comme chaque service peut avoir un public différent, il est possible de choisir plusieurs valeurs.
- "tou.te.s" : il n'y a aucun critère pour utiliser certains services ;
- "adheren.te.s" : certains services sont réservés aux adhérent.e.s ;
- "client.e.s" : certains services ne peuvent être utilisés que par celles et ceux qui le paient ;
- "cercle\_prive" : certains services ne sont accessibles que sur cooptation.

### servers
La liste des URL des sites des hébergeurs du Chaton.

### status
C'est le statut du Chaton dans le collectif, il peut être :
- "non\_verifie" ;
- "evaluation\_en\_cours" ;
- "approuve" ;
- "conflit\_potentiel" ;
- "mediation\_demandee" ;
- "retire".

### metadata
Un objet pour toutes les informations utiles qui ne rentrent pas dans les autres champs.

## Détail des champs d'un service
### type
C'est le nom du service, ce qu'il permet de faire. La liste des services possibles a été tirée du site chatons.org, avec quelques ajouts signalés en *italique*. Si un des services que vous proposez n'est pas dans la liste, n'hésitez pas à faire une merge request. En attendant, vous pouvez utiliser la valeur "autre" et mettre une description du service.

- "mail"
- "contacts" : carnet d'adresses
- "mailing_list" : pour des échanges par mail
- "newsletter" : pour diffuser une information
- "audioconference"
- "visioconference"
- "messagerie_instantanee" : messagerie avec une information organisée, de type Mattermost ou Slack
- *"serveur_messagerie_instantanee"* : serveur de messagerie instantanée comme Matrix
- *"client_messagerie_instantanee"* : client de messagerie instantanée comme Riot
- "chat" : messagerie orientée vers la discussion personne à personne et sans structuration des discussions, type Jabber ou Messenger
- "reseau_social" : par exemple Diaspora*, alternatives à Facebook
- "microblogging" : par exemple Mastodon, alternatives à Twitter
- *"client_microblogging"* : frontend pour Twitter
- "moteur_recherche"
- "cartographie" : type OpenStreetMap
- "sondage" : pour des formulaires complexes comme pour la recherche universitaire par exemple, alternatives à SurveyMonkey
- "reduc_url"
- "transfert_chiffre" : alternative à Pastebin, transférer du texte ou du code avec possibilité de le chiffrer
- "favoris" : gestionnaire de marque-pages
- "rss" : agrégateur de flux
- "lire_plus_tard" : sauvegarde de contenus web
- "mots_de_passe" : gestionnaire de mots de passe
- "forge_logicielle"
- "agenda" : calendrier partagé
- "doc_collaboratif" : outil de rédaction collaborative
- "tab_collaboratif" : tableur collaboratif
- "whiteboard_collaboratif" : tableau blanc
- "diapo_collaboratif"
- "questionnaire" : pour des formulaires plus simples, alternatives à Google Forms
- "rdv" : prise de rendez-vous, alternatives à Doodle
- "schema" : création de schémas et diagrammes
- *"outils_svg"* : pour optimiser des images
- "carte_heuristique" : création des mind maps
- "taches"
- "pad_collaboratif" : outil de prise de note collaborative
- "gestion_projet"
- "prise_decision" : aide à la prise de décision, alternatives à Klaxoon
- "stockage_fichiers" : pour stocker et partager durablement des fichiers
- "partage_temporaire_fichier" : pour héberger temporairement un fichier afin de le partager, alternatives à WeTransfer
- "partage_image" : alternatives à Imgur
- "partage_album_photo" : alternatives à Flickr
- "diffusion_video" : alternatives à YouTube
- "diffusion_audio" : alternatives à Soundcloud
- "diffusion_direct" : pour l'audio ou la vidéo, alternatives aux lives YouTube et Facebook
- "blog" : hébergement de sites webs et de blogs, Wordpress
- "comptabilite" : gestionnaire de finances personnelles
- "facturation" : gestionnaire de facturation et de paiement
- "progiciel_gestion" : progiciel de gestion intégrée, ou ERP
- "mutualise" : service d'hébergement sur serveur(s) mutualisé(s)
- "vps" : location de serveur(s) virtuel(s)
- "dedie" : location de serveur(s) dédié(s)
- "vpn" : fournisseur de vpn
- *"resolveur_doh"* : résolveur DNS over HTTPS
- "autre"

### description
Une courte description du service ou des informations utiles sur son utilisation.

### software
Le logiciel qui rend ce service. Les valeurs possibles viennent aussi du site du CHATONS avec quelques ajouts :
- "codimd"
- "cryptpad"
- *"cyberchef"*
- "darkwire.io"
- "draw.io"
- "easyrtc"
- "ethercalc"
- "etherpad"
- "filetolink"
- "firefoxsend"
- "framadate"
- *"gist"*
- *"gitlab"*
- *"gitea"*
- "jitsi_meet"
- "libreto"
- "lstu"
- "lufi"
- "lutim"
- "mattermost"
- "mumble"
- "mycryptochat"
- *"nextcloud"*
- *"peertube"*
- "plik"
- "polr"
- "privatebin"
- *"prosody"*
- "rocket_chat"
- "rs_short"
- "scrumblr"
- *"searx"*
- "shaarli"
- "strut"
- *"svgomg"*
- *"tiny_tiny_rss"*
- *"wallabag"*
- *"wekan"*

### url
Un lien d'accès direct au service.

### account
S'il est nécessaire de créer ou pas un compte.
- "sans\_inscription" ;
- "sur\_inscription".

### public
Le public qui peut utiliser le service. Les valeurs possibles sont les mêmes que pour le public du Chaton :
- "tou.te.s" : il n'y a aucun critère pour utiliser le service ;
- "adheren.te.s" : le service est réservé aux adhérent.e.s ;
- "client.e.s" : le service ne peut être utilisé que par celles et ceux qui le paient ;
- "cercle\_prive" : le service n'est accessible que sur cooptation.

### price
Si le service est payant ou gratuit. Un service est considéré comme gratuit s'il n'est pas nécessaire de payer spécifiquement pour l'utiliser. Par exemple, un service destiné seulement aux adhérent.e.s peut être considéré comme gratuit même si l'adhésion est payante dès lors que le service ne demande pas de payer en plus.

Cette information peut servir à construire le modèle économique du Chaton. Les valeurs possibles sont :
- "gratuit" ;
- "payant" ;
- "prix\_libre" ;
- "freemium" : certaines fonctionnalités sont accessibles à tou.te.s et d'autres nécessitent de payer.

### degradability
Pour combien de temps les données sont gardées (sur un pad ou un hébergement d'images par exemple), en jours. Pour des données gardées pour toujours, mettre 0.

### maintained_till
Si une fin est programmée pour le service, la renseigner ici. Le champ prend des dates au format "AAAA-MM-JJ".

### weight
Un service peut être presque saturé ou bien, au contraire, être capable de supporter une charge lourde. Le champ `weight` permet de renseigner cette information pour, par exemple, répartir la charge d'utilisateur.ice.s entre diférentes instances d'un même service.

L'échelle va de 1 (service presque saturé) à 10 (service qui peut accueillir encore de nombreuses personnes). Mettre 5 par défaut.
